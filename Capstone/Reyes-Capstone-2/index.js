// Links and libraries
	const express = require("express")
	const mongoose = require("mongoose")
	const cors = require("cors")

	const userRoute = require("./routes/userRoute")
	const productRoute = require("./routes/productRoute")

	const app = express()

// Connect to MongoDB Atlas
	mongoose.connect("mongodb+srv://reyeslukevictor:tYRurrZGLNYnO2cz@wdc028-course-booking.nxmr7cb.mongodb.net/EcommerceAPI",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)

// Connect to MongoDb locally
	mongoose.connection.once("open", () => console.log("Online and connected to Capstone Server"))

// Middleware
	app.use(express.json())
	app.use(express.urlencoded({extended: true}))

	app.use(cors())
	app.use("/users", userRoute)		//http://localhost:4000/users
	app.use("/products", productRoute)	//http://localhost:4000/products

app.listen(process.env.PORT || 4001, () => console.log(`Now listening to port ${process.env.PORT || 4001}`))
const Product = require("../models/Product")

// Controller to Create a product
	module.exports.addProduct = (data) => {
		if (data.isAdmin) {
			let newProduct = new Product({
				name : data.product.name,
				description : data.product.description,
				price : data.product.price
			})
			
			return newProduct.save().then((product, error) => {
			
				if (error) {
					return false;
				}	else {
					return true;
				}
			})

		}

		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		})
	}

// Controllers for retrieving all the products
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result
		})
	}

// Controllers for retrieving active products
	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result
		})
	}

// Controllers for retrieving specific products
	module.exports.getProduct = (reqParams) => {
		return Product.findById(reqParams.productId).then(result => {
			return result
		})
	}

// Controllers for updating a product
	module.exports.updateProduct = (productId, data) => {
		if (data.isAdmin) {
			let updatedProduct = {
				name: data.product.name,
				description: data.product.description,
				price: data.product.price
			}

			return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
				if (error) {
					return false;
				} else {
					return true;
				}
			})
		}

		let message = Promise.resolve("User must be Admin to access this.")
		return message.then((value) => {
			return {value}
		})
	}

// Controllers for archiving a product
	module.exports.archiveProduct = (productId, data) => {
		if (data.isAdmin) {
			let updateActiveField = {
				isActive: false
			}

			return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
				if(error) {
					return false;
				}	else {
					return true;
				}
			})
		}

		let message = Promise.resolve("User must be Admin to access this.")
			return message.then((value) => {
				return {value}
			})
	}

// Controllers to Activate Product
	module.exports.activateProduct = (productId, data) => {
		if(data.isAdmin) {
			let updateActiveField = {
				isActive: true
			}

			return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {
				if(error) {
					return false;
				}	else {
					return true;
				}
			})
		}

		let message = Promise.resolve("User must be Admin to access this.")
			return message.then((value) => {
				return {value}
			})
	}
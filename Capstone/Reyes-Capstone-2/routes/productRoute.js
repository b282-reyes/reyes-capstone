const express = require("express");

const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");

// Route for product course
    router.post("/create", auth.verify, (req, res) => {

      const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
      }

      productController.addProduct(data).then(resultFromController => res.send(resultFromController));
    })

// Route for retrieving all the products
    router.get("/all", (req, res) => {
      productController.getAllProducts().then(resultFromController => res.send(resultFromController))
    })

// Route for retrieving active products
    router.get("/active", (req, res) => {
      productController.getAllActive().then(resultFromController => res.send(resultFromController))
    })

// Route for retrieving specific products
    router.get("/:productId", (req, res) => {
      productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
    })

// Route for updating a product ADMIN
    router.put("/:productId/update", auth.verify, (req, res) => {
      const productId = req.params.productId
      const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
      }

      productController.updateProduct(productId, data).then(resultFromController => res.send(resultFromController))
    })

// Route for archiving a product ADMIN
  router.patch("/:productId/archive", auth.verify, (req, res) => {
    const productId = req.params.productId
    const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(productId, data).then(resultFromController => res.send(resultFromController))
  })

// Route activate a product ADMIN
  router.patch("/:productId/activate", auth.verify, (req, res) => {
    const productId = req.params.productId;
    const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.activateProduct(productId, data).then(resultFromController => res.send(resultFromController))
  })








module.exports = router;
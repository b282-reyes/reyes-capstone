const express = require("express")

const router = express.Router()

const userController = require("../controllers/userController")

const auth = require("../auth")

// Routes for checking if the user's email already exist in the database
		router.post("/checkEmail", (req,res) => {
				userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
		})

// Route for user registration
		router.post("/register", (req, res) => {
				userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
		})

// Route for user authentication (login)
		router.post("/login", (req, res) => {
				userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
		})

		router.get("/details", auth.verify, (req, res) => {
				const userData = auth.decode(req.headers.authorization)

				userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
		})

// Route for Non Admin user Check out
		router.post("/checkout", auth.verify, (req, res) => {
				const userId = auth.decode(req.headers.authorization).id
				const data = {
						userId: userId,
						productId: req.body.productId,
						productName: req.body.productName, 
				    quantity: req.body.quantity,
				    totalAmount: req.body.totalAmount
			  }

				userController.checkout(data, userId)
				    .then(resultFromController => res.send(resultFromController))
				    .catch(error => {
					      console.error(error);
					      res.status(500).send("Not allowed to checkout.")
				    })
		})

module.exports = router;

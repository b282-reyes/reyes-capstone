const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
    email: {
      type: String,
      required: [true, "Email is required"],
    },

    password: {
      type: String,
      required: [true, "Password is required"],
    },

    isAdmin: {
      type: Boolean,
      default: false,
    },

    orderedProduct: [
      {
        products: [
          {
            productId: {
              type: String,
              required: [true, "Product ID is required"],
            },

            productName: {
              type: String,
              required: [true, "Product name is required"],
            },

            quantity: {
              type: Number,
              required: [true, "Quantity is required"],
            },
          },
        ],

        totalAmount: {
          type: Number,
          required: [true, "Amount is required"],
        },
        
        purchasedOn: {
          type: Date,
          default: new Date(),
        },
      },
    ],
})

module.exports = mongoose.model("User", userSchema)

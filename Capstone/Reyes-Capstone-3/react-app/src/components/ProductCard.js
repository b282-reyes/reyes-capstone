import {useState, useEffect} from 'react';

import { Button, Row, Col, Card } from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function ProductCard({product}) {
    const {name, description, price, _id} = product;



return (

    <Row className="mt-3 mb-3">
        <Col xs={12}>
            <Card className="cardHighlight p-0" className="bg-dark">
                <Card.Body>
                    <Card.Title className="text-light"><h4>{name}</h4></Card.Title>
                    <br/>

                    <Card.Subtitle className="text-light">Description: {description}</Card.Subtitle>
                    <Card.Text className="text-light"></Card.Text>

                    <Card.Subtitle className="text-light">Price: {price === 0? 'Free' : `₱${price}`}</Card.Subtitle>
                    <Card.Text className="text-light"></Card.Text>

                    <Button variant="warning" className="" as={Link} to={`/products/${_id}`}>Details</Button>
                </Card.Body>
            </Card>
        </Col>
    </Row>       
    )
}



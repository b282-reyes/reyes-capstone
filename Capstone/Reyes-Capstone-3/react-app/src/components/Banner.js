import { Link } from 'react-router-dom'

import { Button, Row, Col } from 'react-bootstrap'

import '../designs/Banner.css'

export default function Banner({data}) {
    const {title, content, destination, label} = data

    return (
        <Row>
            <Col className="d-flex justify-content align-items-center m-5 banner-container hombrew-bg">
                <div className="text-center text-light">
                    <Button variant="warning" as={Link} to={destination} > 
                        <h1 className="fw-bold"> {title}  </h1>
                        <h3> {content} </h3> 
                    </Button>

                </div>
            </Col>
        </Row>
    )
}


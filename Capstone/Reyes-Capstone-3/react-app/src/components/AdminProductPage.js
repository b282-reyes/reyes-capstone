import React from 'react';

import { Link, useParams } from 'react-router-dom';

import { Button, Row, Col, Card } from 'react-bootstrap';

export default function AdminProductPage({ product, setProducts, products }) {
    const { name, description, price, _id, isActive } = product;

    const {productId} = useParams();

    const archiveProduct = () => {  
        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({ 
                'isAdmin': true,

            })
        })
        .then((res) => res.json())
        .then((data) => {
            if (data.value === true) {
                setProducts((prevProducts) =>
                  prevProducts.map((p) => (p._id === _id ? { ...p, isActive: false } : p))
                )
            }
        })
        .catch((error) => {
            console.error('Error archiving the product:', error)
        })
      }

  const activateProduct = () => {
      fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
          method: 'PATCH',
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({ 
              isAdmin: true 
          })
      })
      .then((res) => res.json())
      .then((data) => {
          if (data === true) {
              setProducts((prevProducts) => prevProducts.map((p) => (p._id === _id ? { ...p, isActive: true } : p)))
          }
      })
      .catch((error) => {
          console.error('Error activating the product:', error)
      })
  }

  return (
      <Row className="mt-3 mb-3">
          <Col xs={12}>
              <Card className="cardHighlight p-3 bg-dark">
              <Card.Body>
                  <Card.Title className="fw-bold text-light"> <h2>{name}</h2> </Card.Title>
                  
                  <br/>
                          
                  <Card.Subtitle className="text-light">Description:</Card.Subtitle>
                  <Card.Text className="text-light">{description}</Card.Text>
                  
                  <br/>
                  <br/>

                  <Card.Subtitle className="text-light">Price: {price === 0? 'Free' : `₱${price}`}</Card.Subtitle>
                  <Card.Text className="text-light"></Card.Text>
                  
                  <br/>

                  <Card.Subtitle className="text-light">Update Product</Card.Subtitle>
                  <Button className="bg-primary" as={Link} to={`/products/${_id}/update`}> Update </Button>
                  
                  <br/>
                  <br/>

                  <Card.Subtitle className="text-light">Update Status</Card.Subtitle>
                  <Button 
                      variant={isActive ? 'success' : 'secondary'} 
                      onClick={isActive ? archiveProduct : activateProduct} > {isActive ? 'Active' : 'Archived'} 
                  </Button>
                  
              </Card.Body>
              </Card>
          </Col>
      </Row>
  )
}

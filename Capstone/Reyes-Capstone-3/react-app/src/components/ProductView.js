import { useState, useContext, useEffect } from 'react'

import { useParams, Link, useNavigate } from 'react-router-dom'

import { Container, Card, Button, Row, Col } from 'react-bootstrap'

import Swal from 'sweetalert2'

import UserContext from '../UserContext'

export default function ProductView() {
    const { user } = useContext(UserContext)
    const navigate = useNavigate()
    const { productId } = useParams()

    const [productName, setProductName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)
    const [quantity, setQuantity] = useState(1)
    const [totalAmount, setTotalAmount] = useState(0)

    const handleQuantityChange = (event) => {
        const newQuantity = parseInt(event.target.value, 10)
        setQuantity(newQuantity)
        setTotalAmount(price * newQuantity)
    }

    const checkout = () => {
        const requestBody = {
            productId: productId,
            productName: productName,
            quantity: quantity,
            totalAmount: totalAmount,
        }

        fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(requestBody),
        })
        .then((res) => res.json())
        .then((data) => {
            console.log('Checkout response:', data);

            if (data) {
                Swal.fire({
                    title: 'Checkout Successful!',
                    icon: 'success',
                    text: 'You have placed an order.',
                })

              navigate('/products');
            }   else {
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again.',
                })
            }
        })
        .catch((error) => {
            console.error('Error while checking out:', error)

            Swal.fire({
                title: 'Error',
                icon: 'error',
                text: 'Something went wrong. Please try again later.',
            })
        })
    }

        useEffect(() => {
            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
                .then((res) => res.json())
                .then((data) => {
                    console.log(data)
              
                        setProductName(data.name)
                        setDescription(data.description)
                        setPrice(data.price)
                        setTotalAmount(data.price * quantity)
                })
                .catch((error) => {
                    console.error('Error fetching product details:', error)
            })
        }, [productId, quantity]);

    return (
        <Container>
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card className="m-5 bg-dark">
                        <Card.Body className="p-4 m-5">
                            <Card.Title className="fw-bold text-light">{productName}</Card.Title>

                            <Card.Text className="text-light"> {description} </Card.Text>
                            <br/>
                            <br/>

                            <Card.Subtitle className="text-light"> Price: {price === 0? 'Free' : `₱${price}`} </Card.Subtitle>
                            <br/>

                            <Card.Subtitle className="text-light">Quantity: <input type="number" value={quantity} onChange={handleQuantityChange} /> </Card.Subtitle>                          
                            <br/>                          

                            <Card.Subtitle className="text-light">Total Amount: {price === 0? 'Free' : `₱${price}`} </Card.Subtitle>
                            <br/>
                            <br/>
                            <br/>

                            { user.id !== null ? (
                                <Button variant="warning" onClick={() => checkout(productId)}> Checkout </Button>
                            ) 

                            : 

                            (
                                <Button className="btn btn-danger" as={Link} to="/login"> Log in to Checkout </Button>
                            )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

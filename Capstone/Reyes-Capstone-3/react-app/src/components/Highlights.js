import{Row, Col, Card} from 'react-bootstrap'

import '../designs/Highlights.css'

export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3 card-with-seeker-bg black-tint-overlay"> 
	                <Card.Body>
	                    <Card.Title className="text-white"> <h2 className="fw-bold"> Browse Games: Where the realm of gaming comes alive! </h2> </Card.Title>
	                    <Card.Text className="text-white"> <h7> Whether you're an avid gamer seeking new challenges or a casual player looking for some quality leisure time, our diverse and expansive collection has something for everyone. </h7> </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>

	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3 card-with-entertainer-bg black-tint-overlay">
	                <Card.Body>
	                    <Card.Title className="text-white"> <h2 className="fw-bold"> Steamers Zone: Where Gaming Meets Entertainment </h2> </Card.Title>
	                    <Card.Text className="text-white"> <h7> Step into the Streamer Zone and discover a dynamic collection of games handpicked for your enjoyment. Whether you're a strategy enthusiast, a role-playing fanatic, or a fan of pulse-pounding action, our diverse selection ensures there's a game that perfectly suits your tastes. </h7> </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>

	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3 card-with-tinkerer-bg black-tint-overlay">
	                <Card.Body>
	                    <Card.Title className="text-white"> <h2 className="fw-bold"> ForgeHub: Forge Your Worlds </h2> </Card.Title>
	                    <Card.Text className="text-white"> <h7> Your ultimate destination for unleashing your creative prowess and crafting digital wonders! Here, we empower you to become a game-changer by providing a cutting-edge platform to design, develop, and share your very own mods, games, and tools. </h7></Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>

	    </Row>
	)
}


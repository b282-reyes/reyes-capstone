import { React, useContext } from 'react'

import { Nav, Navbar } from 'react-bootstrap'

import { Link, NavLink } from 'react-router-dom'

import UserContext from '../UserContext'

import '../designs/AppNavbar.css'


function AppNavbar() {
    const { user } = useContext(UserContext)

    return (
        <Navbar expand="lg" className="bg-dark">
        
                <Navbar.Brand as={Link} to="/" className="navbar-brand dwarf-logo"></Navbar.Brand>
                
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                    <Nav.Link as={NavLink} to="/products" className="text-light fw-bold m-2">Store</Nav.Link>

                    { (user.id) ?
                        <Nav.Link as={NavLink} to="/logout" className="text-light fw-bold m-2">Logout</Nav.Link>

                        :

                        <>
                            <Nav.Link as={NavLink} to="/login" className="text-light fw-bold m-2">Login</Nav.Link>
                            <Nav.Link as={NavLink} to="/register" className="text-light fw-bold m-2">Register</Nav.Link>
                        </>
                      }
                    </Nav>
                </Navbar.Collapse>

        </Navbar>
  )
}

export default AppNavbar
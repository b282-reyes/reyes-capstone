import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home() {
	const data = {
	   	title: "Anvil",
	    content: "Embrace your Dwarf Needs! Check out our Store",
	    destination: "/products",
	    label: "Check it out!"
	}

  	return (
	    <>
		    <Banner data={data} />
		    <Highlights />
	    </>
  	)
}
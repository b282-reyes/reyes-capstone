import Banner from '../components/Banner'

import { Row, Col } from 'react-bootstrap'

export default function Error() {

    const data = {
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (
        <Row>
            <Col className="d-flex justify-content align-items-center p-5 banner-container">
                <Banner data={data} />
            </Col>
        </Row>
    )
}

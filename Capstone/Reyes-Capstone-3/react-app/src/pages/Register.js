import {useState, useEffect, useContext} from 'react';

import { Navigate, useNavigate } from 'react-router-dom';

import { Container,Form, Button, Card, Row, Col } from 'react-bootstrap'

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register() {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if((email !== "" && 
            password1 !== "" && 
            password2 !== "") &&
            (password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password1, password2])

    function registerUser(e) {     
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {
                Swal.fire({
                    title: "Duplicate Email Found",
                    icon: "error",
                    text: "Kindly provide another email to complete registration."
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        email: email,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data === true) {
                        setEmail("")
                        setPassword1("")
                        setPassword2("")

                        Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Play Well, Well Played"
                        })

                        navigate("/login")
                    } else {
                        Swal.fire({
                            title: "Error",
                            icon: "error",
                            text: "Please, try again."
                        })
                    }
                })
            }
        })
    }


    return (
        (user.id !== null)?
        <Navigate to ="/products" />

        :
        <Col lg={{ span: 6, offset: 3 }}>
            <Card className="m-5 p-5 bg-dark">
                <Form onSubmit={(e) => registerUser(e)} >

                    <h1 className="text-center my-3 text-light">Registration</h1>
                        <Form.Group className="mb-3" controlId="userEmail">
                            <Form.Label className="d-flex justify-content-center text-light">Email address</Form.Label>
                                <Form.Control 
                                    type="email"
                                    value={email}
                                    onChange={(e) => {setEmail(e.target.value)}} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password1">
                            <Form.Label className="d-flex justify-content-center text-light">Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    value={password1}
                                    onChange={(e) => {setPassword1(e.target.value)}} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="password2">
                            <Form.Label className="d-flex justify-content-center text-light">Verify Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    value={password2}
                                    onChange={(e) => {setPassword2(e.target.value)}} />
                        </Form.Group>

                          { isActive ?
                                    <Button variant="success" type="submit" id="submitBtn"> Submit </Button>

                                    :

                                    <Button variant="warning" type="submit" id="submitBtn" disabled> Submit </Button>
                          }         
                </Form>
            </Card>
        </Col>
    )
}
import React, { useState, useEffect } from 'react'
import Button from 'react-bootstrap/Button'
import AdminProductPage from '../components/AdminProductPage'
import { Link } from 'react-router-dom'

export default function AdminPanel() {
  const [products, setProducts] = useState([])

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/products/all`)
          .then(res => res.json())
          .then(data => {
              setProducts(data)
        })
        .catch(error => {
            console.error('Error fetching product data:', error)
        })
  }, [])

   return (
    <>
      <br />

        <h1 className="text-center"> Admin Dashboard </h1>
        <br/>
          <div className="d-grid gap-2">
              <Link to="/products/new">
                  <Button variant="primary" className="fw-bold px-5 py-2"> Add New Product </Button>
              </Link>
          </div>
      

      {products.map((product) => (
          <AdminProductPage key={product._id} product={product} setProducts={setProducts} products={products} />
      ))}
    </>
  )
}

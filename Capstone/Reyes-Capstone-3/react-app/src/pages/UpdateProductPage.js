import React, { useState, useEffect } from 'react'

import { useNavigate, Link, useParams } from 'react-router-dom'

import { Form, Container, Button, Row, Col } from 'react-bootstrap'

import Swal from 'sweetalert2'

export default function UpdateProductPage() {
    const navigate = useNavigate()
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState("")
    const {productId} = useParams()


useEffect(() => {   
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
        method: 'GET',
    })
    .then((res) => res.json())
    .then((data) => {
       
        if (data && Object.keys(data).length > 0) {
            console.log(data)
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price.toString())
        }   else {
            console.error("Invalid or empty product data received.")
        }
    })
    .catch((error) => {
        console.error("Error fetching product details:", error)
    })
  }, [productId])

    function updateProduct(e) {
        e.preventDefault()

        const productData = {
            'name': name,
            'description':description,
            'price': parseFloat(price),
        }

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(productData),
        })
        .then((res) => res.json())
        .then((data) => {
            console.log(data)

            if (data === true) {
                setName("")
                setDescription("")
                setPrice("")

                Swal.fire({
                  title: "Product Updated",
                  icon: "success",
                  text: "Product has been updated.",
                });

                navigate("/admin_dashboard")
            }   else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please, try again.",
                })
            }
        })
        .catch((error) => {
            console.error("Error updating product:", error)

            Swal.fire({
                title: "Error",
                icon: "error",
                text: "Something went wrong.",
            })
        })
    }

    return (
        <Container>
            <Row>
                <Col lg={{ span: 6, offset: 3 }}> 
                    <div className="container mt-5">
                        <h1 className="fw-bold text-center">Update Product</h1>

                        <br/>

                        <Form onSubmit={updateProduct}>
                            <Form.Group controlId="productName">
                                <Form.Label className="fw-bold">Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <br/>

                            <Form.Group controlId="productDescription">
                                <Form.Label className="fw-bold">Product Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    value={description}
                                    onChange={(e) => setDescription(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <br/>

                            <Form.Group controlId="productPrice">
                                <Form.Label className="fw-bold">Product Price</Form.Label>
                                <Form.Control
                                    type="number"
                                    value={price}
                                    onChange={(e) => setPrice(e.target.value)}
                                    step="0.01"
                                    required
                                />
                            </Form.Group>

                            <br/>

                            <Button className="fw-bold" variant="primary" type="submit"> Update? </Button>
                        </Form>
                    </div>
                </Col>    
            </Row>
        </Container>
    )
}
